# Nodezoo GitHub Service

## Introduction

### Pre-requsites

* Java 11

Nodezoo GitHub Service fetches package information from GitHub repositories.

## Development
The generation of the executable jar file can be performed by issuing the following command

```shell
make package
```

This will create an executable jar file **nodezoo-github-service.jar** within the _build/libs_ gradle folder. This can
be started by executing the following command

```shell
./gradlew :app:libertyRun --no-daemon
```

### Liberty Dev Mode

During development, you can use Liberty's development mode (dev mode) to code while observing and testing your changes on the fly.
With the dev mode, you can code along and watch the change reflected in the running server right away; 
unit and integration tests are run on pressing Enter in the command terminal; you can attach a debugger to the running server at any time to step through your code.

```shell
make app_dev_run
```

To stop Liberty running in the background, use:
```shell
./gradlew :app:libertyStop
```

To launch the test page, open your browser at the following URL: http://localhost:8080/index.html  

## Specification examples

More information on MicroProfile can be found [here](https://microprofile.io/)

## Development with Kubernetes

Prepare the application docker image and push to docker, assuming `docker login` already done.
```shell
make dev_app_image
make push_dev_app_image
```

### Local K8s like minikube

Assuming `kubectl` has the right context:
```shell
java -version 2>&1 | head -n1 # check Java 11
make dev_dist dev_deploy
```

### EKS
Assuming `kubectl` has the EKS context:
```shell
java -version 2>&1 | head -n1 # check Java 11
make dev_eks_dist dev_deploy
```

## Building Gitlab CD
```shell
make ci_image
```
This will create the CI image with your configured docker host. Push this image to Docker Hub.


## Production

### Install
Prepare the application docker to be pushed to ECR. The default AWS region is `us-east-1`. To change to a different
region, create a `.makerc` file in the repository directory and add the following entries.

```makefile
# Change AWS region
AWS_REGION := ap-south-1

# Use an aws-cli profile
#AWS_PROFILE := cdk
```

Make a new package, create the ECR repository, create an app image and push the image to ECR.
```shell
make clean package app_repository app_image push_app_image
```

### Deploy
For a production setup, deploy does not deploy the manifests to a K8s cluster. Instead, it submits it to a FLux repository.
Check the `nodezoo-flux` and `nodezoo-k8s-manifests` projects for setting them up correctly before trying a
production setup of this service.
```shell
java -version 2>&1 | head -n1 # check Java 11
make dist deploy
```
With the `gitops` setup with GitLab CI, pushing a git tag starting with `aws-rel` will trigger a deployment.

## Useful commands

* `make app_dev_run`   run dev server with auto-reload
* `make test`          run all tests
* `make package`       make the app package for copying to docker
* `make dist`          emits the synthesized CloudFormation template
* `make deploy`        deploy this stack to your default AWS account/region

Enjoy!

