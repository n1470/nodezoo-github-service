package tao.nodezoo.github.core;

import javax.json.JsonObject;

/**
 * Fetch package information.
 */
public interface InfoService {
    /**
     * @param packageName package name to fetch information
     * @return Json API response
     */
    JsonObject fetchPackageInfo(String packageName);
}
