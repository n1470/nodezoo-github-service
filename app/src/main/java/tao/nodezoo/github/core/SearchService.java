package tao.nodezoo.github.core;

/**
 * Enables searching for packages.
 */
public interface SearchService {
    /**
     * @param query package to search for
     * @return full name of most relevant search result
     */
    String findPackage(String query);
}
