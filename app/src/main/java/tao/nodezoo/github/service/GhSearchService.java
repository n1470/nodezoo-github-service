package tao.nodezoo.github.service;

import org.jetbrains.annotations.NotNull;
import tao.nodezoo.github.core.SearchService;
import tao.nodezoo.github.http.GhClient;
import tao.nodezoo.github.parser.SearchResponseParser;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Map;

@Singleton
public class GhSearchService implements SearchService {

    private static final String REQUEST_PATH = "/search/repositories";
    private GhClient ghClient;

    @Inject
    public void setGhClient(GhClient ghClient) {
        this.ghClient = ghClient;
    }

    /**
     * <pre>
     *     https://api.github.com/search/repositories\?q\=react
     * </pre>
     *
     * @param query package to search for
     * @return first matching package full_name field
     */
    @Override
    public String findPackage(@NotNull String query) {
        final var inputStream = ghClient.get(REQUEST_PATH, Map.of("q", query));
        final var parser = new SearchResponseParser(inputStream);
        final var match = parser.findFirstNameMatch(query);
        if (match != null) return match.getString("full_name");

        return null;
    }
}
