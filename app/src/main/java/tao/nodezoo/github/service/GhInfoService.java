package tao.nodezoo.github.service;

import tao.nodezoo.github.core.InfoService;
import tao.nodezoo.github.http.GhClient;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.json.Json;
import javax.json.JsonObject;
import java.util.Map;

@Singleton
public class GhInfoService implements InfoService {

    private GhClient ghClient;

    @Inject
    public void setGhClient(GhClient ghClient) {
        this.ghClient = ghClient;
    }

    /**
     * <pre>
     *     https://api.github.com/repos/reactjs/reactjs.org
     * </pre>
     * @param packageName package name to fetch information
     * @return Json object representing package information
     */
    @Override
    public JsonObject fetchPackageInfo(String packageName) {
        final var inputStream = ghClient.get("/repos/" + packageName, Map.of());
        return Json.createReader(inputStream).readObject();
    }
}
