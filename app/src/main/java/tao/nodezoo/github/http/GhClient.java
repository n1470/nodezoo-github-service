package tao.nodezoo.github.http;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;

public class GhClient {

    private final static String DEFAULT_BASE_URL = "https://api.github.com";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private String apiBaseUrl = DEFAULT_BASE_URL;

    public void setApiBaseUrl(String apiBaseUrl) {
        this.apiBaseUrl = apiBaseUrl;
    }

    /**
     * Get request and returns the input stream.
     *
     * <pre>
     * curl \
     * -H "Accept: application/vnd.github+json" \
     * -H "X-GitHub-Api-Version: 2022-11-28" \
     * https://api.github.com/search/repositories\?q\=react
     * </pre>
    */
    public InputStream get(@NotNull String requestPath,
                           @NotNull Map<String, String> queryParams) {
        final var client = HttpClient.newHttpClient();
        final var query = queryParams.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue())
                .reduce("", (a, e) -> a + e + "&").replaceFirst("&$", "");
        final var uriBuilder = new StringBuilder();
        uriBuilder.append(apiBaseUrl).append(requestPath);
        if (!query.isEmpty()) uriBuilder.append('?').append(query);

        final var uriPath = uriBuilder.toString();
        logger.debug("uriPath: {}", uriPath);
        final var request = HttpRequest.newBuilder()
                .uri(URI.create(uriPath))
                .setHeader("Accept", "application/vnd.github+json")
                .setHeader("X-GitHub-Api-Version", "2022-11-28")
                .build();
        try {
            final HttpResponse<InputStream> response = client.send(request, HttpResponse.BodyHandlers.ofInputStream());
            if (response.statusCode() == 200) {
                return response.body();
            }

            throw new RuntimeException("Request failed with status code:" + response.statusCode());
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
