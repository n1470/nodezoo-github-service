package tao.nodezoo.github;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 */
@ApplicationPath("/app")
public class NodezooGithubServiceRestApplication extends Application {
}
