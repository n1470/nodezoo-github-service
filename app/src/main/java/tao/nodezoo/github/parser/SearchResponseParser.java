package tao.nodezoo.github.parser;

import org.jetbrains.annotations.NotNull;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.stream.JsonParser;
import java.io.InputStream;
import java.util.regex.Pattern;

public class SearchResponseParser {

    private final InputStream inputStream;

    public SearchResponseParser(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public JsonObject findFirstNameMatch(@NotNull String pattern) {
        final var regex = Pattern.compile(pattern);
        try (var parser = Json.createParser(this.inputStream)) {
            while (parser.hasNext()) {
                final var event = parser.next();
                if (event == JsonParser.Event.KEY_NAME && parser.getString().equals("items")) {
                    parser.next();
                    try (var item$ = parser.getArrayStream()) {
                        final var match = item$.filter((v) -> regex
                                        .matcher(v.asJsonObject().getString("full_name"))
                                        .find())
                                .findFirst();
                        if (match.isPresent()) {
                            return match.get().asJsonObject();
                        }
                    }
                }
            }
        }

        return null;
    }
}
