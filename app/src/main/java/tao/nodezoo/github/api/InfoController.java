package tao.nodezoo.github.api;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tao.nodezoo.github.core.InfoService;
import tao.nodezoo.github.core.SearchService;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 */
@Path("/info")
@Singleton
@Produces(MediaType.APPLICATION_JSON)
public class InfoController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private InfoService infoService;
    private SearchService searchService;

    @Inject
    public void setInfoService(InfoService infoService) {
        this.infoService = infoService;
    }

    @Inject
    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    @GET
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "Package was found in GitHub repository",
                    content = @Content(mediaType = "application/json")
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "No matches for package",
                    content = @Content(mediaType = "text/plain")
            )
    })
    @Operation(
            summary = "Fetches the package information from GitHub",
            description = "Searches for the repositories and returns information on the best match"
    )
    public Response queryPackage(
            @Parameter(
                    description = "Package to search and get information",
                    required = true,
                    example = "react",
                    schema = @Schema(type = SchemaType.STRING)
            )
            @QueryParam("q") String query) {
        logger.debug("GET /info?q={}", query);
        final var packageName = searchService.findPackage(query);
        if (packageName == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        return Response.ok().entity(infoService.fetchPackageInfo(packageName).toString()).build();
    }
}
