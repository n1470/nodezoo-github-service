package tao.nodezoo.github.api;

import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.jupiter.api.Test;
import tao.nodezoo.github.core.InfoService;
import tao.nodezoo.github.core.SearchService;

import javax.json.Json;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Application;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class InfoControllerTest extends JerseyTest {

    final SearchService searchService = mock(SearchService.class);
    final InfoService infoService = mock(InfoService.class);

    @Test
    void packageLookupSuccess() {
        final var pkgFullName = "facebook/react";
        when(searchService.findPackage(anyString())).thenReturn(pkgFullName);
        final Map<String, Object> objectMap = Map.of("id", 105963253);
        final var res = Json.createObjectBuilder(objectMap).build();
        when(infoService.fetchPackageInfo(eq(pkgFullName))).thenReturn(res);
        final Invocation.Builder request = target().path("/info").queryParam("q", "react").request();
        final var response = request.get();
        final var resBody = response.readEntity(String.class);
        assertEquals(200, response.getStatus());
        assertEquals(res.toString(), resBody);
        assertEquals("application/json", response.getHeaderString("Content-Type"));
    }

    @Test
    void packageLookupFailure_404_Response() {
        when(searchService.findPackage(anyString())).thenReturn(null);
        final Invocation.Builder request = target().path("/info").queryParam("q", "gizmodo").request();
        final var response = request.get();
        assertEquals(404, response.getStatus());
    }

    @Override
    protected Application configure() {
        final ResourceConfig resourceConfig = new ResourceConfig(InfoController.class);
        resourceConfig.register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(searchService).to(SearchService.class);
                bind(infoService).to(InfoService.class);
            }
        });
        return resourceConfig;
    }
}
