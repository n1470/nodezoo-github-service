package tao.nodezoo.github.http;

import org.junit.jupiter.api.Test;
import org.mockserver.client.MockServerClient;
import org.mockserver.model.Header;
import org.testcontainers.containers.MockServerContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import javax.json.Json;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

@Testcontainers
class GhClientTest {

    public static final DockerImageName MOCKSERVER_IMAGE = DockerImageName
            .parse("mockserver/mockserver")
            .withTag("mockserver-" + MockServerClient.class.getPackage().getImplementationVersion());

    @Container
    public MockServerContainer mockServer = new MockServerContainer(MOCKSERVER_IMAGE);

    @Test
    void testGet() throws IOException {
        try (var mockServerClient = new MockServerClient(mockServer.getHost(), mockServer.getServerPort())) {
            final var resourcePath = "/tao/nodezoo/github/parser/search_response.json";
            try (InputStream resource = this.getClass().getResourceAsStream(resourcePath)) {
                assert resource != null;
                mockServerClient
                        .when(request().withPath("/search/repositories")
                                .withQueryStringParameter("q", "react")
                                .withHeader(Header.header("Accept", "application/vnd.github+json"))
                                .withHeader(Header.header("X-GitHub-Api-Version", "2022-11-28")))
                        .respond(response().withBody(resource.readAllBytes()));

                final var client = new GhClient();
                client.setApiBaseUrl(mockServer.getEndpoint());
                final var inputStream = client.get("/search/repositories", Map.of("q", "react"));
                final var jsonObject = Json.createReader(inputStream).readObject();
                assertEquals(3400869, jsonObject.getInt("total_count"));
                assertEquals("facebook/react",
                        jsonObject.getJsonArray("items").get(0).asJsonObject().getString("full_name"));
            }
        }
    }
}