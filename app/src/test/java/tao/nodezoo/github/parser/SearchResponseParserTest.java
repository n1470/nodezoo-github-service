package tao.nodezoo.github.parser;

import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

public class SearchResponseParserTest {

    @Test
    void testExactFullNameMatch() throws IOException {
        final SearchResponseParser parser;
        try (var inputStream = this.getClass().getResourceAsStream("search_response.json")) {
            parser = new SearchResponseParser(inputStream);
            final var match = parser.findFirstNameMatch("react");
            assertNotNull(match);
        }
    }

    @Test
    void testNoMatch() {
        final var noMatchOutput = "{\n" +
                "  \"total_count\": 0,\n" +
                "  \"incomplete_results\": false,\n" +
                "  \"items\": [\n" +
                "\n" +
                "  ]\n" +
                "}";
        final var inputStream = new ByteArrayInputStream(noMatchOutput.getBytes(StandardCharsets.UTF_8));
        final var parser = new SearchResponseParser(inputStream);
        final var match = parser.findFirstNameMatch("zigmodo");
        assertNull(match);
    }
}
