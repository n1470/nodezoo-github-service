package tao.nodezoo.github.service;

import org.junit.jupiter.api.Test;
import tao.nodezoo.github.core.SearchService;
import tao.nodezoo.github.http.GhClient;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class GhSearchServiceTest {

    @Test
    void findPackage() throws IOException {
        final SearchService service = new GhSearchService();
        final var ghClient = mock(GhClient.class);
        try (var inputStream = this.getClass().getResourceAsStream("partial_search_response.json")) {
            when(ghClient.get(anyString(), anyMap())).thenReturn(inputStream);
            ((GhSearchService) service).setGhClient(ghClient);
            final var matchingPackage = service.findPackage("react");
            assertEquals("facebook/react", matchingPackage);
        }
    }

    @Test
    void findPackage_no_match() {
        final SearchService service = new GhSearchService();
        final var ghClient = mock(GhClient.class);
        final var noMatchOutput = "{\n" +
                "  \"total_count\": 0,\n" +
                "  \"incomplete_results\": false,\n" +
                "  \"items\": [\n" +
                "\n" +
                "  ]\n" +
                "}";
        final var inputStream = new ByteArrayInputStream(noMatchOutput.getBytes(StandardCharsets.UTF_8));
        when(ghClient.get(anyString(), anyMap())).thenReturn(inputStream);
        ((GhSearchService) service).setGhClient(ghClient);
        final var matchingPackage = service.findPackage("gizmodo");
        assertNull(matchingPackage);
    }
}
