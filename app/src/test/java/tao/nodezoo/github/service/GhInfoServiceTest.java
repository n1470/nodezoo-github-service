package tao.nodezoo.github.service;

import org.junit.jupiter.api.Test;
import tao.nodezoo.github.core.InfoService;
import tao.nodezoo.github.http.GhClient;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;

class GhInfoServiceTest {

    @Test
    void fetchPackageInfo() throws IOException {
        final InfoService service = new GhInfoService();
        final var ghClient = mock(GhClient.class);
        try (var inputStream = this.getClass().getResourceAsStream("get_response.json")) {
            when(ghClient.get(anyString(), anyMap())).thenReturn(inputStream);
            ((GhInfoService) service).setGhClient(ghClient);
            final var getResponse = service.fetchPackageInfo("facebook/react");
            assertNotNull(getResponse);
            assertEquals(105963253, getResponse.getInt("id"));
            assertEquals("reactjs", getResponse.getJsonObject("owner").getString("login"));
        }
    }
}
